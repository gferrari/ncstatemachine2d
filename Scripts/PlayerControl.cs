using UnityEngine;
using System.Collections;

public class PlayerControl : MonoBehaviour
{
	private GameData gameDataRef;

	//Settings
	public float maxSpeed = 7f;
	bool facingRight = true;
	public Transform groundCheck;
	bool grounded = false;
	float groundRadius = 0.2f;
	public LayerMask whatIsGround;
	public float jumpForce = 250f;
	public Rigidbody2D projectile;

	//Custom player
	public Color red = Color.red;
	public Color blue = Color.blue;
	public Color green = Color.green;
	public Color yellow = Color.yellow;
	public Color white = Color.white;

	//Special Powers
	public bool canFire = false;

	void Start ()
	{
		gameDataRef = GameObject.Find(Tags.gameController).GetComponent<GameData>();
	}

	void Update()
	{

	}

	void FixedUpdate(){
		//GroundCheck
		grounded = Physics2D.OverlapCircle (groundCheck.position, groundRadius, whatIsGround);
		if (!grounded) return;
	}
	

	void OnTriggerEnter2D(Collider2D other)
	{
		if(other.gameObject.tag == Tags.collect)
		{
			gameDataRef.score += 1;
			Destroy(other.gameObject);
		}
		if(other.gameObject.tag == Tags.enemy)
		{
			gameDataRef.playerLives -= 1;
			Destroy(other.gameObject);
		}
	}

	//Powers
	public void FireProjectile()
	{
		Rigidbody2D clone;

		//TODO: Optimice
		if (facingRight) 
		{
			Vector3 newPosition = transform.position;
			newPosition.x = transform.position.x + 1;
			newPosition.y = transform.position.y;

			clone = Instantiate(projectile, newPosition, transform.rotation) as Rigidbody2D;			
			Vector3 theScale = transform.localScale;
			theScale.x *= 1;
			clone.transform.localScale = theScale;
			clone.rigidbody2D.velocity = transform.TransformDirection (Vector2.right * 10);
		}
		else 
		{
			Vector3 newPosition = transform.position;
			newPosition.x = transform.position.x - 1;
			newPosition.y = transform.position.y;

			clone = Instantiate(projectile, newPosition, transform.rotation) as Rigidbody2D;
			
			Vector3 theScale = transform.localScale;
			theScale.x *= 1;
			clone.transform.localScale = theScale;
			clone.rigidbody2D.velocity = transform.TransformDirection (-Vector2.right * 10);
		}
	}

	//Invert the Sprite Player
	void Flip()
	{
		facingRight = !facingRight;
		Vector3 theScale = transform.localScale;
		theScale.x *= -1;
		transform.localScale = theScale;
	}

	//Move left
	public void Jump()
	{
		if (grounded) 
		{
			rigidbody2D.AddForce (new Vector2 (0, jumpForce));
		}
	}


	//Move left
	public void MoveRight()
	{
		rigidbody2D.velocity = new Vector2 ( maxSpeed, rigidbody2D.velocity.y);		
		if (!facingRight) 
		{
			Flip ();
		} 
	}

	//Move right
	public void MoveLeft()
	{
		rigidbody2D.velocity = new Vector2 ( -maxSpeed, rigidbody2D.velocity.y);		
		if (facingRight) 
		{
			Flip ();
		} 
	}

	//Select Color for player or you can change this for change Sprite Component
	public void PickedColor (Color playerColor)
	{
		renderer.material.color = playerColor;
	}

	//Enable or disable Powers
	public void CanFire(bool canFireEnabled)
	{
		canFire = canFireEnabled;
	}
}