﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof (Rigidbody2D))]
[RequireComponent(typeof(BoxCollider2D))]
public class PadController : MonoBehaviour {
	
	// GUI textures
	public GUITexture guiLeft;
	public GUITexture guiRight;
	public GUITexture buttonA;
	public GUITexture buttonB;

	// Movement variables
	public float moveSpeed = 5f;
	public float jumpForce = 50f;
	public float maxJumpVelocity = 2f;

	private GameObject player;
	private PlayerControl playerController;
	
	// Movement flags
	private bool moveLeft, moveRight, doJump, actionA,actionB = false;
	
	void Start ()
	{
		player = GameObject.Find (Tags.player);
		playerController = player.GetComponent<PlayerControl> ();
	}
	// Update is called once per frame
	void Update () {
		
		// Check to see if the screen is being touched
		if (Input.touchCount > 0)
		{
			for(int i = 0; i < Input.touchCount; i++) {
				// Get the touch info
				Touch t = Input.GetTouch(i);
				
				// Did the touch action just begin?
				if (t.phase == TouchPhase.Began)
				{
					// Are we touching the left arrow?
					if (guiLeft.HitTest(t.position, Camera.main))
					{
						Debug.Log("Touching Left Control");
						moveLeft = true;
					}
					
					// Are we touching the right arrow?
					if (guiRight.HitTest(t.position, Camera.main))
					{
						Debug.Log("Touching Right Control");
						moveRight = true;
					}


					// Are we touching the A button?
					if (buttonA.HitTest(t.position, Camera.main))
					{
						Debug.Log("Touching A Button");
						actionA = true;
					}

					// Are we touching the B button?
					if (buttonB.HitTest(t.position, Camera.main))
					{
						Debug.Log("Touching B Button");
						actionB = true;
					}
				}
				
				// Did the touch end?
				if (t.phase == TouchPhase.Ended)
				{
					// Stop all movement
					doJump = moveLeft = moveRight = actionA = actionB = false;
				}
			}
		}
		
		// Is the left mouse button down?
		if (Input.GetMouseButtonDown(0))
		{
			// Are we clicking the left arrow?
			if (guiLeft.HitTest(Input.mousePosition, Camera.main))
			{
				Debug.Log("Touching Left Control");
				moveLeft = true;
			}
			
			// Are we clicking the right arrow?
			if (guiRight.HitTest(Input.mousePosition, Camera.main))
			{
				Debug.Log("Touching Right Control");
				moveRight = true;
			}

			// Are we clicking the A button?
			if (buttonA.HitTest(Input.mousePosition, Camera.main))
			{
				Debug.Log("Touching A Button");
				actionA = true;
			}
			// Are we clicking the B button?
			if (buttonB.HitTest(Input.mousePosition, Camera.main))
			{
				Debug.Log("Touching B Button");
				actionB = true;
			}
		}
		
		if (Input.GetMouseButtonUp(0))
		{
			// Stop all movement on left mouse button up
			doJump = moveLeft = moveRight = actionA = actionB = false;
		}


		//Keyboard
		if (Input.GetKeyDown (KeyCode.UpArrow)) 
		{
			Debug.Log("Touching Jump Control");
			doJump = true;		
		}
		else if (Input.GetKeyUp (KeyCode.UpArrow)) 
		{
			doJump = false;
		}

		if (Input.GetKeyDown (KeyCode.LeftArrow)) 
		{
			Debug.Log("Touching Left Control");
			moveLeft = true;			
		}
		else  if (Input.GetKeyUp (KeyCode.LeftArrow)) 
		{
			moveLeft = false;
		}

		if (Input.GetKeyDown (KeyCode.RightArrow)) 
		{
			Debug.Log("Touching Right Control");
			moveRight = true;			
		}
		else  if (Input.GetKeyUp (KeyCode.RightArrow)) 
		{
			moveRight = false;
		}

		if (Input.GetKeyDown (KeyCode.LeftControl)) 
		{
			Debug.Log("Touching A Button");
			actionA = true;			
		}
		else  if (Input.GetKeyUp (KeyCode.LeftControl)) 
		{
			actionA = false;
		}

		if (Input.GetKeyDown (KeyCode.LeftShift)) {
			Debug.Log ("Touching B Button");
			actionB = true;			
		} 
		else  if (Input.GetKeyUp (KeyCode.LeftShift)) 
		{
			actionB = false;
		}


	}
	
	void FixedUpdate()
	{
		//anim.SetFloat("Speed", Mathf.Abs);
		// Set velocity based on our movement flags.
		if (moveLeft)
		{			
			playerController.MoveLeft();
		}
		
		if (moveRight)
		{
			playerController.MoveRight();
		}
		
		if (doJump)
		{
			playerController.Jump();
		}

		if (actionA) 
		{
			playerController.Jump();
		}

		if (actionB) 
		{
			if(playerController.canFire)
			{
				playerController.FireProjectile();
			}
		}


	}
}