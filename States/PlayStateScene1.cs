﻿using UnityEngine;
using Assets.Code.Interfaces;


namespace Assets.Code.States {
	public class PlayStateScene1 : IStateBase  {

		private StateManager manager;
		private GameObject player;
		private PlayerControl playerController;

		public PlayStateScene1(StateManager managerRef)
		{
			manager = managerRef;
			if (Application.loadedLevelName != "Scene1") 
			{
				Application.LoadLevel ("Scene1");
			}

			player = GameObject.Find (Tags.player);
			player.rigidbody2D.isKinematic = false;
			playerController = player.GetComponent<PlayerControl> ();
			//Activate Powers
			playerController.CanFire (true);
		}

		public void StateUpdate()
		{
			if (manager.gameDataRef.playerLives <= 0) 
			{
				manager.SwitchState (new LostStateScene1 (manager));
				player.transform.position =  new Vector2(0, 2);
				manager.gameDataRef.ResetPlayer();
				player.rigidbody2D.isKinematic = true;
			}

			if (manager.gameDataRef.score >= 2) 
			{
				manager.SwitchState (new WonStateScene1 (manager));
				player.transform.position =  new Vector2(0, 2);
				player.rigidbody2D.isKinematic = true;
			}
		}

		public void ShowIt()
		{
			GUI.Box(new Rect(10,10,100,25), string.Format("Score: "+ manager.gameDataRef.score));
			GUI.Box(new Rect(Screen.width - 110,10,100,25), string.Format("Lives left: "+  manager.gameDataRef.playerLives));
		}

	}
}
