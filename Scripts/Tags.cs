﻿using UnityEngine;
using System.Collections;

public class Tags : MonoBehaviour
{
	// A list of tag strings.
	public const string player = "Player";
	public const string collect = "Collect";
	public const string terrain = "Terrain";
	public const string gameController = "GameManager";
	public const string enemy = "Enemy";
}