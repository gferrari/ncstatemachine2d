﻿using UnityEngine;
using System.Collections;

public class EnergyPulsePower : MonoBehaviour
{
	public float pulseDuration = 1f;
	
	public Transform Collect;
	
	void Update()
	{
		pulseDuration -= Time.deltaTime;

		if(pulseDuration <= 0)
			Destroy(gameObject);
	}
	
	void OnTriggerEnter2D(Collider2D other)
	{
		//To enter in this collider we need the projectile object are a collider
		if(other.gameObject.tag == "Enemy")
		{
			Instantiate(Collect, other.transform.position,other.transform.rotation);
			//GameObject.Find("GameManager").GetComponent<GameData>().playerLives += 1;
			Destroy(other.gameObject);
			Destroy(gameObject);
		}
    	else
		{
      		Destroy(gameObject);
  		}
	}
}